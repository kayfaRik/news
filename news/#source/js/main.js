/*banner-info*/
$(function(){
    $('.banner-info__questions h2').click(function(){
        $(this).next().slideToggle();
        $('#hide').css({'transform':'rotate(-90deg)','transform-origin':'top center'});
    });
    //Ссылка в соц сети
    $('.btn-menu__link').click(function(e){
        $(this).next().slideToggle();
        $(this).toggleClass('close-link').fadeIn();
    });


    // Лента сайта
    $('.btn-menu__tape').click(function(){
        $(this).css({'background-color':'#ffffff', 'color':'#13202c','transition':'all 1s'});
        $(this).next().slideDown();
        $('section,main').addClass('blur');
        $('body').css({'overflow':'hidden'});
        $('.navigation__title').css({'display':'none'});
        $('html,body').stop();
    });
    $('.btn-menu__close-dropdown-tape').click(function(){
        $('.btn-menu__dropdown-tape').slideUp();
        $('main,section').removeClass('blur');
        $('body').css({'overflow':'visible'});
        $('.navigation__title').css({'display':'block'});
        $('.btn-menu__tape').css({'background-color':'#13202c', 'color':'#ffffff'});
    })
    

    // прокрутка страницы
        $('.start').on('click', function(e){
          $('html,body').stop().animate({ scrollTop: $('#start').offset().top }, 1000);
          e.preventDefault();
        });
});
//triagle
// $(function(){
//     $('.banner-info__questions h2').click(function(){
//         $(this).next().css({'transform':'rotate(-90deg)','transform-origin':'top center'});
//     });
// });

